%### Terms of Use
%--------------
% This Hierarchical Rank Pooling software is strictly for non-commercial academic use only. 
% This Hierarchical Rank Pooling code or any modified version of it may not be used for any commercial activity, such as:
%1. Commercial production development, Commercial production design, design validation or design assessment work.
%2. Commercial manufacturing engineering work
%3. Commercial research.
%4. Consulting work performed by academic students, faculty or academic account staff
%5. Training of commercial company employees.

%### License
%-------
% The analysis work performed with the program(s) must be non-proprietary work. 
% Licensee and its contract users must be or be affiliated with an academic facility. 
% Licensee may additionally permit individuals who are students at such academic facility 
% to access and use the program(s). Such students will be considered contract users of licensee. 
% The program(s) may not be used for commercial competitive analysis 
% (such as benchmarking) or for any commercial activity, including consulting.

% *************************************************************************
% Dependency : Lib Linear 
% Author : Basura Fernando (basura.fernando@anu.edu.au)
function w = liblinearsvr(Data,C,normD)
    if normD == 2
        Data = normalizeL2(Data);
    end    
    if normD == 1
        Data = normalizeL1(Data);
    end    
    N = size(Data,1);
    Labels = [1:N]';
    model = train(double(Labels), sparse(double(Data)),sprintf('-c %1.6f -s 11 -q',C) );
    w = model.w';    
end
